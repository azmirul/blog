@extends('layouts.app')

@section('content')


<div class="col-md-6 col-md-offset-3">

  <div class="panel panel-default">
    <div class="panel-heading">{{$title}} <a class="btn btn-success" href="{{route('blog.create')}}">create</a></div>
    <div class="panel-body">
      <table class="table table-bordered">
        <tr>
          <th>no.</th>
          <th>Title</th>
          <th>Content</th>
          <th>Acion</th>
        </tr>
        @php $count=1; @endphp
        @foreach($blog as $b)
        <tr>
          <th>{{$count++}}</th>
          <th>{{$b->title}}</th>
          <th>{{$b->content}}</th>
          <th>
            <a href="{{route('blog.edit',$b->id)}}" class="btn btn-success">update</a>
            <form method="post" action="{{route('blog.destroy',$b->id)}}">
              {{method_field('delete')}}
              {{ csrf_field() }}
              <button type="submit" class="btn btn-danger">delete</button>
            </form>
          </th>
        </tr>
        @endforeach



    </div>
  </div>

</div>

@endsection
