@extends('layouts.app')

@section('content')


<div class="col-md-6 col-md-offset-3">

  <div class="panel panel-default">
    <div class="panel-heading">{{$title}}</div>
    <div class="panel-body">

      <form class="form-horizontal" method="post" action="{{$action}}">
        @if(isset($method))
        {{method_field($method)}}
        @endif
        {{ csrf_field() }}
        <div class="form-group">
          <label for="inputEmail3" class="col-sm-2 control-label">Title</label>
          <div class="col-sm-10">
            <input name="title" type="text" class="form-control" placeholder="Title" value="{{$blog->title or null}}">
          </div>
        </div>
        <div class="form-group">
          <label for="inputPassword3" class="col-sm-2 control-label">Content</label>
          <div class="col-sm-10">
            <textarea name="content" class="form-control" placeholder="Content">{{$blog->content or null}}</textarea>
          </div>
        </div>

        <div class="form-group">
          <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" class="btn btn-default">Submit</button>
          </div>
        </div>
      </form>

    </div>
  </div>

</div>

@endsection
